pipeline
{
    agent any
    
    stages
    {
    
        stage('Job Describe')
        {
            steps
            {
                echo "##########################################"
                echo "##########################################"
                echo ""
                echo "BRANCH_NAME = ${BRANCH_NAME}"
                echo "BUILD_NUMBER = ${BUILD_NUMBER}"
                echo "BUILD_ID = ${BUILD_ID}"
                echo "BUILD_TAG = ${BUILD_TAG}"
                echo ""
                echo "##########################################"
                echo "##########################################"
            }
        }
        
        /*
        stage('Versioning') 
        {
            steps
            {
                script
                {
                    //déterminer l'extension
                    
		    if (${BRANCH_NAME} == "developpement" ){
		      extension = "-SNAPSHOT"
		    }
		    if (${BRANCH_NAME} == "master" ){
		      extension = ""
		    }

		    // Récupération du commitID long
		    def commitIdLong = sh returnStdout: true, script: 'git rev-parse HEAD'

		    //Récupération du commitID court
		    
		    def commitId = commitIdLong.take(7)

		    //Modification de la version dans le pom.xml
		    sh "sed -i s/'-XXX'/${extension}/g pom.xml"

		    //Ajout du nom du conteneur dans application properties
		    sh "sed -i s/'XXX'/${ipPostgres}/g src/main/resources/application.properties"

		    /Récupération de la version du pom.xml après modification
		    def version = sh returnStdout: true, script: "cat pom.xml | grep -A1 '<artifactId>application' | tail -1 |perl -nle 'm{.*<version>(.*)</version>.*};print \$1' | tr -d '\n'"
                }
            }
        }
        */
        
        stage('Git checkout docker_compose_generator') 
        {
            steps
            {
                script
                {
                    // checkout to the docker_compose_generator Git repository
                    git checkout: 'master', url: 'https://gitlab.com/projet_devops_1_mpham/docker_compose_generator.git'
                }
            }
        }
        
        
        stage('Run postgres container')
        {
            steps
            {
                sh "./main.sh -p"
                sh "docker ps -a"
                sh "./main.sh -i"
            } 
        }

        stage('Git checkout application') 
        {
            steps
            {
                script
                {
                    // checkout to the appication Git repository
                    git checkout: '${BRANCH_NAME}', url: 'https://gitlab.com/projet_devops_1_mpham/application.git'
                }
            }
        }
        
        /* Maven - tests */
        /*
        stage('Maven test') 
        {
            steps
            {
                script
                {
                    sh 'docker run --rm --name maven-${commitIdLong} -v /var/lib/jenkins/maven/:/root/.m2 -v "$(pwd)":/usr/src/mymaven --network generator_generator -w /usr/src/mymaven maven:3.3-jdk-8 mvn -B clean test'
                }
            }
        }*/
        
        /* Maven - deploy */
        stage('Maven deploy') 
        {
            steps
            {
                script
                {
                    sh 'docker run --rm --name maven-${commitIdLong} -v /var/lib/jenkins/maven/:/root/.m2 -v "$(pwd)":/usr/src/mymaven --network generator_generator -w /usr/src/mymaven maven:3.3-jdk-8 mvn -B -s /usr/src/mymaven/.m2/settings.xml clean deploy'
                }
            }
        }
        
        stage('Docker build image') 
        {
            steps
            {
                script
                {
                
                    docker.withRegistry('https://gitlab.com/projet_devops_1_mpham/application/container_registry', ${CI_REGISTRY_TOKEN})
                    {
                        def customImage = docker.build("first image") 
                        //customImage.push()
                    }
                }
            }
        }
        
        
        
        stage('Clean containers')
        {
            steps
            { 
                sh "./main.sh -c" 
            } 
        }
        
        
    }
}
